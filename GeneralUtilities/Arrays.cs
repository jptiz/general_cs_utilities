using System;
using System.Runtime.CompilerServices;

namespace Tiz
{
    public static class Arrays
    {
        /// <summary>
        /// Finds mininum value in array.
        /// </summary>
        /// <remarks>O(n) complexity.</remarks>
        /// <param name="original">The array</param>
        /// <returns>Mininum value in array.</returns>
        public static T Min<T>(T[] original) where T : IComparable
        {
            var min = original[0];
            foreach (var value in original)
            {
                if (value.CompareTo(min) < 0) min = value;
            }
            return min;
        }

        /// <summary>
        /// Finds maximum value in array.
        /// </summary>
        /// <remarks>O(n) complexity.</remarks>
        /// <param name="original">The array</param>
        /// <returns>Maximum value in array.</returns>
        public static T Max<T>(T[] original) where T : IComparable
        {
            var max = original[0];
            foreach (var value in original)
            {
                if (value.CompareTo(max) > 0) max = value;
            }
            return max;
        }

        /// <summary>
        /// Fills array with value.
        /// </summary>
        /// <param name="original">The array</param>
        /// <param name="value">The value</param>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static void Fill<T>(T[] original, T value)
        {
            for (var i = 0; i < original.Length; i++)
            {
                original[i] = value;
            }
        }

        /// <summary>
        /// Normalizes array to values from 0 to 1.
        /// </summary>
        /// <remarks>O(n) complexity</remarks>
        public static void Normalize<T>(T[] original) where T: IComparable
        {
            dynamic max = Max(original);
            for (var i = 0; i < original.Length; i++)
            {
                original[i] = original[i] / max;
            }
        }

        /// <summary>
        /// Replaces range with data.
        /// </summary>
        /// <typeparam name="T">Arrays type</typeparam>
        /// <param name="array">Source array</param>
        /// <param name="index">Starting index</param>
        /// <param name="data">Data</param>
        public static void Replace<T>(T[] array, int index, T[] data)
        {
            for (var i = 0; i < data.Length; i++)
            {
                array[index + i] = data[i];
            }
        }
    }
}