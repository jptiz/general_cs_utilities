﻿using System.Runtime.CompilerServices;

namespace Tiz
{
    /// <summary>
    /// General operations.
    /// </summary>
    public static class General
    {
        /// <summary>
        /// Swaps two variables' values.
        /// </summary>
        /// <remarks>O(1) complexity.</remarks>
        /// <param name="a">Value to be swapped</param>
        /// <param name="b">Value to be swapped</param>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static void Swap<T>(T a, T b)
        {
            var a_ = a;
            a = b;
            b = a_;
        }
        /// <summary>
        /// Struct specialization of <see cref="Swap{T}(T, T)"/>.
        /// </summary>
        /// <remarks>O(1) complexity.</remarks>
        /// <param name="a">Value to be swapped</param>
        /// <param name="b">Value to be swapped</param>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static void Swap<T>(ref T a, ref T b) where T : struct
        {
            var a_ = a;
            a = b;
            b = a_;
        }
    }
}
