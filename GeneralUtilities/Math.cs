﻿namespace Tiz.Math
{
    public static class RadianExtensions
    {
        /// <summary>
        /// Converts degrees to radians.
        /// </summary>
        /// <param name="degrees">Angle (in degrees)</param>
        /// <returns>Angle (in radians)</returns>
        public static double ToRadians(this double degrees)
        {
            return degrees * System.Math.PI / 180.0;
        }

        /// <summary>
        /// Converts degrees to radians.
        /// </summary>
        /// <param name="degrees">Angle (in degrees)</param>
        /// <returns>Angle (in radians)</returns>
        public static float ToRadians(this float degrees)
        {
            return (float)(degrees * System.Math.PI / 180.0);
        }
    }
}
