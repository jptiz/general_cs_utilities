﻿/// <summary>
/// Includes set of types and methods for geometric representations.
/// </summary>
namespace Tiz.Geometry
{
    /// <summary>
    /// 2D point of double coordinates.
    /// </summary>
    public class Point2D
    {
        public double X { get; set; } = 0;
        public double Y { get; set; } = 0;

        public Point2D()
        {
        }

        public static Point2D From(dynamic other)
        {
            return new Point2D { X = other.X, Y = other.Y };
        }

        public void Move(double x, double y)
        {
            X += x;
            Y += y;
        }

        public static Point2D operator +(Point2D left, Point2D right)
        {
            return new Point2D { X = left.X + right.X, Y = left.Y + right.Y };
        }

        public static Point2D operator -(Point2D left, Point2D right)
        {
            return new Point2D { X = left.X - right.X, Y = left.Y - right.Y };
        }
    }

    /// <summary>
    /// 3D point of double coordinates.
    /// </summary>
    public class Point3D
    {
        public double X { get; set; } = 0;
        public double Y { get; set; } = 0;
        public double Z { get; set; } = 0;

        public Point3D()
        {
        }

        public void Move(double x, double y, double z)
        {
            X += x;
            Y += y;
            Z += z;
        }

        public static Point3D operator +(Point3D left, Point3D right)
        {
            return new Point3D {
                X = left.X + right.X,
                Y = left.Y + right.Y,
                Z = left.Z + right.Z,
            };
        }

        public static Point3D operator -(Point3D left, Point3D right)
        {
            return new Point3D {
                X = left.X - right.X,
                Y = left.Y - right.Y,
                Z = left.Z - right.Z,
            };
        }

        public static Point3D operator +(Point3D left, Point2D right)
        {
            return new Point3D
            {
                X = left.X + right.X,
                Y = left.Y + right.Y,
                Z = left.Z,
            };
        }

        public static Point3D operator -(Point3D left, Point2D right)
        {
            return new Point3D
            {
                X = left.X - right.X,
                Y = left.Y - right.Y,
                Z = left.Z,
            };
        }
    }
}
